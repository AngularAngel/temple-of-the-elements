extends SecondaryAttribute

class_name PercentileAttribute

export var inverse = false;

func display_score(score, max_length = 10, min_length = 0):
	var display = str(score*100).substr(0, max_length - 1);
	if display.ends_with("."):
		display = display.substr(0, display.length() - 1);
	while display.length() < min_length - 1:
		display = " " + display;
	return display + "%";

func calculate():
	var value = 100 / stat_sum() * multiplier;
	if inverse:
		return 1 / value;
	else:
		return value;

func get_base_potential():
	var value = 0.0;
	for dependency in dependencies.keys():
		value += dependency.potential.get_cur_score() * dependencies[dependency];
	if value > 0:
		value = 100 / value * multiplier;
		if inverse:
			value = 1 / value;
	return calc_cur_score(value);