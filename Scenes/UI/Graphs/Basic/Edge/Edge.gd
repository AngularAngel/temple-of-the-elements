extends Control

class_name Edge

var start;
var end;

func _ready():
	$Appearance.set_default_color(Color(1, 1, 1));

func set_start(vertex):
	vertex.add_edge_out(self);
	start = vertex;
	set_start_position(vertex.get_position());

func set_start_position(position):
	if $Appearance.get_points().size() < 1:
		$Appearance.add_point(position);
	else:
		$Appearance.set_point_position(0, position);

func set_end(vertex):
	vertex.add_edge_in(self);
	end = vertex;
	set_end_position(vertex.get_position());

func set_end_position(position):
	if $Appearance.get_points().size() < 1:
		$Appearance.add_point(Vector2(0, 0));
	
	if $Appearance.get_points().size() < 2:
		$Appearance.add_point(position);
	else:
		$Appearance.set_point_position(1, position);

func update():
	if end != null:
		end.update();
