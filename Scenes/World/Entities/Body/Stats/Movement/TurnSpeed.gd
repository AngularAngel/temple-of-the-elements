extends SecondaryAttribute

func _ready():
	add_dependency("Dexterity", 0.2);
	add_dependency("Agility", 0.5);
	add_dependency("Vitality", 0.2);
	add_dependency("Spirit", 0.1);
	multiplier = 0.05;
	recalculate();