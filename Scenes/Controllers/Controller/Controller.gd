extends Node2D

class_name Controller

signal died

func _ready():
	$Body.controller = self;

func _on_Body_died(body):
	emit_signal("died", self);
	queue_free();