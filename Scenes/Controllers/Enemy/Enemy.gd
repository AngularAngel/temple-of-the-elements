extends Controller

# Member variables
var turn = 0;
var direction = Vector2(0, -1);
var xp = 10;

func _ready():
	connect("died", get_parent().get_parent(), "_on_Enemy_died");
	$Body/Attributes.get_stat("Strength").set_base_score(10);
	$Body/Attributes.get_stat("Dexterity").set_base_score(10);

func get_turn():
	return turn;
	
func get_direction():
	return direction.rotated($Body/Physics.get_global_rotation());

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	turn += rand_range(-1, 1) * delta;
	if turn > 1: turn = 1;
	if turn < -1: turn = -1;
	
	var sight = $Body/Physics/Ray.get_collider();
	if sight != null and sight.is_player():
		$Body.attack();

func is_player():
	return false;
