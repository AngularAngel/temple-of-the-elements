shader_type canvas_item;

//uniform sampler2D tex0;
//uniform float border; // 0.01
uniform float circle_radius; // 0.5
//uniform vec4 circle_color; // vec4(1.0, 1.0, 1.0, 1.0)
uniform vec2 circle_center; // vec2(0.5, 0.5)

float circle(in vec2 _st, in float _radius){
    vec2 dist = _st-vec2(0.5, 0.5);
	return 1.-smoothstep(_radius-(_radius*0.01),
                         _radius+(_radius*0.01),
                         dot(dist,dist)*4.0);
}

void fragment() {
	vec2 st = FRAGCOORD.xy/ (1.0 / SCREEN_PIXEL_SIZE).xy;

	vec3 color = vec3(circle(st, circle_radius));

	COLOR = vec4( color, 1.0 );
}