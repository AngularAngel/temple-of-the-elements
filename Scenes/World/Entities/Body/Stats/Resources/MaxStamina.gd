extends SecondaryAttribute

func _ready():
	add_dependency("Vitality", 0.4);
	add_dependency("Endurance", 0.35);
	add_dependency("Spirit", 0.25);
	recalculate();