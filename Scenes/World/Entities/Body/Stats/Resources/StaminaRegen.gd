extends SecondaryAttribute

func _ready():
	add_dependency("Vitality", 0.4);
	add_dependency("Spirit", 0.4);
	add_dependency("MaxStamina", 0.2);
	multiplier = 0.1;
	recalculate();