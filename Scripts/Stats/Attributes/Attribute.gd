extends Stat

class_name Attribute

#Member variables
export var display_name: String setget , get_display_name;
var _current_score: float setget set_cur_score, get_cur_score;
var _flat_change: float = 0;
var _percent_change: float = 0;

func _ready():
	if display_name == "":
		display_name = get_name();
	get_parent().add_stat(self);

func get_display_name():
	return display_name;

func get_cur_score():
	return _current_score;

func display_score(score, max_length = 10, min_length = 0):
	var display = str(score).substr(0, max_length);
	while display.length() < min_length:
		display = " " + display;
	return display;
	
func get_display_score(max_length = 10, min_length = 0):
	return display_score(get_cur_score(), max_length, min_length);

func adjust_flat_change(change):
	_flat_change += change;
	recalculate();

func set_cur_score(value):
	var old_score = get_cur_score();
	_current_score = calc_cur_score(value);
	emit_signal("changed", self, old_score, get_cur_score());
	
func calc_cur_score(value):
	return (value + _flat_change) * (1 + _percent_change);

func get_stat(stat_name):
	return get_parent().get_stat(stat_name);
	
func add_stat(stat):
	pass;

func recalculate():
	pass;