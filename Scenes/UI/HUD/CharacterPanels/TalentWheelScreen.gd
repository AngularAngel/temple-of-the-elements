extends CharacterPanel

var stats setget link_stats;
var StatBox = preload("res://Scenes/UI/HUD/CharacterPanels/AttributeScreen/AttributeBox.tscn");
onready var talent_graph = $ScreenLayout/ViewportRect/Viewport/TalentGraph;

func link_stats(value):
	stats = value;
	talent_graph.initialize_graph(stats);