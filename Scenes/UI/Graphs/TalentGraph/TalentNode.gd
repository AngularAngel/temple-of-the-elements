extends RequirementNode

class_name TalentNode

var StatBox = preload("res://Scenes/UI/HUD/CharacterPanels/AttributeScreen/AttributeBox.tscn");
var popup;

func _ready():
	for stat in $Talent.stat_increases:
		var stat_box = StatBox.instance();
		stat_box.set_stat(stat);
		$PopupPanel/StatScroll/StatList.add_child(stat_box);
		popup = $PopupPanel;

func pressed():
	if accessible() and not fulfilled():
		fulfilled = true;
		$Talent.hide_potential();
		$Talent.apply();
		update();
		return;

func _input(event):
	if event is InputEventMouseMotion:
		if position_inside(event.get_position()):
			popup.set_position(get_position() + Vector2(0, -popup.rect_size.y));
			remove_child(popup);
			get_parent().add_child(popup);
			popup.raise();
			if not popup.is_visible():
				popup.show();
				if not fulfilled():
					$Talent.show_potential();
			
		else:
			if popup.is_visible():
				popup.hide();
				if not fulfilled():
					$Talent.hide_potential();