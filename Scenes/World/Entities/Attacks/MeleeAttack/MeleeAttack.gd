extends "res://Scenes/World/Entities/Attacks/Attack/Attack.gd"

# Member variables
var distance;

func _physics_process(delta):
	move_local_x(distance/$Duration.get_wait_time() * delta);
	pass;
