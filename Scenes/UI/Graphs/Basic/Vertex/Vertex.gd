extends Control

class_name Vertex

var edges_in = [];
var edges_out = [];

func add_edge_in(edge):
	edges_in.append(edge);
	update();

func add_edge_out(edge):
	edges_out.append(edge);
	update();

func set_position(position):
	.set_position(position);
	for edge in edges_out:
		edge.set_start_position(position)
	
	for edge in edges_in:
		edge.set_end_position(position)