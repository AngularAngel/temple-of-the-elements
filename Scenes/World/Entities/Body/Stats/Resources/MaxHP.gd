extends SecondaryAttribute

func _ready():
	add_dependency("Strength", 0.25);
	add_dependency("Vitality", 0.25);
	add_dependency("Endurance", 0.5);
	recalculate();