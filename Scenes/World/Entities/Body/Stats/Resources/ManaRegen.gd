extends SecondaryAttribute

func _ready():
	add_dependency("Acumen", 0.25);
	add_dependency("Creativity", 0.5);
	add_dependency("MaxMana", 0.25);
	multiplier = 0.2
	recalculate();