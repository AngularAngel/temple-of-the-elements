extends Node

var mouse_position = null;

func transform(event, scaling):
	var old_transform = get_viewport().get_canvas_transform();
	var scalechange = -old_transform.get_scale();
	var new_transform = old_transform.scaled(scaling);
	
	scalechange += new_transform.get_scale();
	var offset = -event.position * scalechange;
	new_transform = new_transform.translated(offset);

	get_viewport().set_canvas_transform(new_transform);

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT: 
			if event.pressed:
				mouse_position = event.position;
			else:
				mouse_position = null;
				
		if event.button_index == BUTTON_WHEEL_UP:
			var old_transform = get_viewport().get_canvas_transform();
			if old_transform.get_scale().x < 1.5:
				transform(event, Vector2(1.05, 1.05));
		
		if event.button_index == BUTTON_WHEEL_DOWN:
			var old_transform = get_viewport().get_canvas_transform();
			if old_transform.get_scale().x > 0.5:
				transform(event, Vector2(0.95, 0.95));
	
	if event is InputEventMouseMotion and mouse_position != null:
		var old_transform = get_viewport().get_canvas_transform();
		
		var vector = event.position - mouse_position;
		vector /= old_transform.get_scale();
		
		var new_transform = old_transform.translated(vector);
		
		get_viewport().set_canvas_transform(new_transform);
		mouse_position = event.position;
		