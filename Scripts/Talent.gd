extends Node

class_name Talent

var stat_increases = [];
var player_stats = null setget set_player_stats;

func set_player_stats(stats):
	player_stats = stats;

func add_stat(stat_name, increase):
	var stat = PrimaryAttribute.new();
	stat.display_name = stat_name;
	stat.set_base_score(increase);
	stat_increases.append(stat)

func show_potential():
	for stat in stat_increases:
		player_stats.get_stat(stat.get_display_name()).potential.adjust_flat_change(stat.get_cur_score());

func hide_potential():
	for stat in stat_increases:
		player_stats.get_stat(stat.get_display_name()).potential.adjust_flat_change(-stat.get_cur_score());

func apply():
	for stat in stat_increases:
		player_stats.get_stat(stat.get_display_name()).adjust_base_score(stat.get_cur_score());