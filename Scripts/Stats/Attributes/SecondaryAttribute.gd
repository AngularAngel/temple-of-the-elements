extends RealAttribute

class_name SecondaryAttribute

var dependencies = {};
var multiplier = 1;

func stat_sum():
	var sum = 0.0;
	for dependency in dependencies.keys():
		sum += dependency.get_cur_score() * dependencies[dependency];
	return sum;

func calculate():
	return multiplier * stat_sum();

func recalculate():
	var temp_score = calc_cur_score(calculate());
	
	var cur_score = get_cur_score();
	if cur_score != temp_score:
		var old_score = cur_score;
		set_cur_score(calculate());

func get_dependency(path):
	return get_stat(path);

func add_dependency(stat, modifier):
	if stat is String:
		stat = get_dependency(stat);
	if stat is Attribute:
		dependencies[stat] = modifier;
		stat.potential.connect("changed", potential, "_on_Dependency_changed");
		stat.connect("changed", self, "_on_Dependency_changed");

func _on_Dependency_changed(stat, old_score, new_score):
	recalculate();

func get_base_potential():
	var sum = 0.0;
	for dependency in dependencies.keys():
		sum += dependency.potential.get_cur_score() * dependencies[dependency];
	return calc_cur_score(sum * multiplier);