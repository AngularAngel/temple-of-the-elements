extends CanvasLayer

func link_body_info(body):
	$CharacterScreens/AttributeScreen.link_attributes(body.get_node("Attributes"));
	$CharacterScreens/TalentWheelScreen.link_stats(body.get_node("Attributes"));
	$ResourceBoxes/HPBar.set_resource(body.get_node("Attributes/Resources/HP"));
	$ResourceBoxes/ManaBar.set_resource(body.get_node("Attributes/Resources/Mana"));
	$ResourceBoxes/StaminaBar.set_resource(body.get_node("Attributes/Resources/Stamina"));
	$XPBar.set_resource(body.get_node("Attributes/Resources/XP"));
	