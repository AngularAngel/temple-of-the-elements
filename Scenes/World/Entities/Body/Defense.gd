extends Node2D

var attributes
var cur_hp;
var hp_regen;
var cur_mana;
var mana_regen;

func _ready():
	attributes = get_node("../../Attributes");
	cur_hp = attributes.get_stat("CurHP");
	hp_regen = attributes.get_stat("HPRegen");
	cur_mana = attributes.get_stat("CurMana");
	mana_regen = attributes.get_stat("ManaRegen");

func take_damage(damage):
	cur_hp.adjust_base_score(-damage);

func _on_HP_empty(hp):
	get_parent().get_parent().die();