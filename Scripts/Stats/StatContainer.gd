extends Stat

class_name StatContainer

var stats = {};
var subsidiary;

func is_subsidiary():
	if subsidiary == null:
		subsidiary = get_parent().get_class().casecmp_to(self.get_class()) == 0;
	
	return subsidiary;

func get_class():
	return "StatContainer";

func add_stat(stat: Attribute):
	stats[stat.get_name()] = stat;
	
	if is_subsidiary():
		get_parent().add_stat(stat);
	else:
		stat.connect("changed", get_parent(), "_on_Stat_changed");

func get_stat(stat_name):
	if stats.has(stat_name):
		return stats[stat_name];
	else:
		return get_parent().get_stat(stat_name);