extends Node


func adjust_close_buttons():
	var i = 1;
	
	var shown = [];
	for child in get_children():
		if child.is_visible():
			shown.append(child);
	
	while i <= shown.size():
		var panel = shown[-i]
		if i > shown.size()/2.0:
			panel.make_left();
		else:
			panel.make_right();
		i += 1;

func open_panel(panel):
	adjust_close_buttons();

func close_panel(panel):
	adjust_close_buttons();