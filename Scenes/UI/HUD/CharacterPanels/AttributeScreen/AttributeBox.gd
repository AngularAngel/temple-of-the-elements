extends HBoxContainer

var stat: Attribute setget set_stat;

func set_stat(value):
	stat = value;
	$StatName.set_text(stat.get_display_name());
	$StatScore.set_text(stat.get_display_score(7));
	$PotentialScore.hide();
	stat.connect("changed", self, "_on_stat_changed");
	if stat.potential != null:
		stat.potential.connect("changed", self, "_on_potential_stat_changed");

func check_potential_difference():
	if $PotentialScore.get_text().casecmp_to($StatScore.get_text()) == 0:
		$PotentialScore.hide();
	else:
		$PotentialScore.show();

func _on_stat_changed(stat, old_score, new_score):
	$StatScore.set_text(stat.get_display_score(7));
	
	check_potential_difference();

func _on_potential_stat_changed(stat, old_score, new_score):
	$PotentialScore.set_text(stat.get_display_score(7));
	
	check_potential_difference();