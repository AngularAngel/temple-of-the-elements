extends CharacterPanel

var attributes setget link_attributes, get_attributes;
var StatBox = preload("res://Scenes/UI/HUD/CharacterPanels/AttributeScreen/AttributeBox.tscn");
onready var stat_list = $ScreenLayout/StatScroll/StatList;

func link_attributes(value):
	attributes = value;
	for attribute in attributes.stats.values():
		var stat_box = StatBox.instance();
		stat_box.set_stat(attribute);
		stat_list.add_child(stat_box);
	
	stat_list.hide();
	stat_list.show();

func get_attributes():
	return attributes;