extends Node2D

class_name Room

signal cleared
signal enemy_died

func _on_Enemy_died(enemy):
	emit_signal("enemy_died", enemy);
	if get_tree().get_nodes_in_group("Enemies").size() == 1:
		cleared();

func cleared():
	emit_signal("cleared", self);