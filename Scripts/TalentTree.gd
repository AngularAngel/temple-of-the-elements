extends Node

class_name TalentTree

var talents;

func generate_talents(stats, depth):
	randomize();
	var layers = [];
	for i in range(depth):
		layers.append(generate_talent_layer(stats, i + 1));
	
	talents = layers;

func generate_talent_layer(stats, size):
	var layer = [];
	for i in range(size):
		layer.append(generate_cluster(stats, generate_cluster_def(stats)));
	
	return layer;

func generate_cluster(stats, def):
	var ret = {};
	ret.entry_talent = generate_end_talent(stats, def);
	ret.talent_lines = [];
	
	for i in range(def.line_stats.size()):
		ret.talent_lines.append(generate_talent_line(stats, def, i));
	
	ret.end_talent = generate_end_talent(stats, def, 2);
	
	return ret;

func generate_end_talent(stats, def, multiplier = 1):
	var end_talent = Talent.new();
	end_talent.set_player_stats(stats);
	end_talent.set_name("Talent");
	if randf() > 0.5:
		end_talent.add_stat(def.main_stat, 10 * multiplier);
	else:
		if def.line_stats.size() == 3 and randf() > 0.5:
			end_talent.add_stat(def.main_stat, 2 * multiplier);
			end_talent.add_stat(def.line_stats[0], 3 * multiplier);
			end_talent.add_stat(def.line_stats[1], 3 * multiplier);
			end_talent.add_stat(def.line_stats[2], 3 * multiplier);
		else:
			end_talent.add_stat(def.main_stat, 2 * multiplier);
			end_talent.add_stat(def.line_stats[0], 4 * multiplier);
			end_talent.add_stat(def.line_stats[-1], 4 * multiplier);
	
	return end_talent;

func generate_talent_line(stats, def, index):
	var talent_line = [];
	for i in range(3):
		var talent = Talent.new();
		talent.set_player_stats(stats);
		talent.set_name("Talent");
		talent.add_stat(def.main_stat, 2);
		talent.add_stat(def.line_stats[index], 8);
		talent_line.append(talent);
	
	return talent_line;

func generate_cluster_def(stats):
	
	#var attributes = [];
	var attributes = ["Strength", "Dexterity", "Agility", "Vitality", "Endurance", 
					  "Intellect", "Acumen", "Creativity", "Focus", "Perception", 
					  "Spirit", "Luck"];
	
	var ret = {};
	ret.line_stats = [];
	
	var num_lines = 2;
	if randf() > 0.8:
		num_lines += 1;
		if randf() > 0.9:
			num_lines += 1;
			if randf() > 0.9:
				num_lines += 1;
	
	for i in range(num_lines):
		var index = randi() % attributes.size();
		ret.line_stats.append(attributes[index]);
		attributes.remove(index);
	
	ret.main_stat = attributes[randi() % attributes.size()];
	
	return ret;