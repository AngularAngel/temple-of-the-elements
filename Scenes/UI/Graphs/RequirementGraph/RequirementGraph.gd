extends Graph

class_name RequirementGraph

var Requirement = preload("res://Scenes/UI/Graphs/RequirementGraph/Requirement/Requirement.tscn");

func link_vertexes(start, end, requirement = Requirement.instance()):
	.link_vertexes(start, end, requirement);

func create_graph():
	link_vertexes($Vertexes/V1, $Vertexes/V2);
	
	link_vertexes($Vertexes/V2, $Vertexes/V3);