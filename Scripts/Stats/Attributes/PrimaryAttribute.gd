extends RealAttribute

class_name PrimaryAttribute

#Member variables
export(float) var _base_score: float = 100.0 setget set_base_score;

func _ready():
	set_cur_score(_base_score);

func set_base_score(new_base):
	if _base_score != new_base:
		_base_score = new_base;
		recalculate();

func adjust_base_score(adjustment):
	set_base_score(_base_score + adjustment);

func recalculate():
	var temp_score = calc_cur_score(_base_score);
	
	var cur_score = get_cur_score();
	if cur_score != temp_score:
		var old_score = cur_score;
		set_cur_score(_base_score);

func get_base_potential():
	return get_cur_score();