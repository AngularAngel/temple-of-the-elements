extends Node

var RoomScene = preload("res://Scenes/World/Rooms/EnemyRoom.tscn");
var PlayerScene = preload("res://Scenes/Controllers/Player/Player.tscn");

func _on_MainMenu_started_new_game():
	$MainMenu.queue_free();
	
	var room = RoomScene.instance();
	add_child(room);
	
	var player = PlayerScene.instance();
	room.get_node("Bodies").add_child(player);
	player.set_position(Vector2(512, 300));
	
