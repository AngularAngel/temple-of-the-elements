extends Controller

func _ready():
	if get_parent().get_parent() != null:
		get_parent().get_parent().connect("cleared", self, "_on_Room_cleared");
		get_parent().get_parent().connect("enemy_died", self, "_on_enemy_died");
	
	$HUD.link_body_info($Body);
	$Body/Attributes/Resources/XP.connect("full", self, "level_up");

func get_turn():
	if Input.is_action_pressed("ui_turn_left"):
		return -1;
	if Input.is_action_pressed("ui_turn_right"):
		return 1;
	return 0;

func get_direction():
	var direction = Vector2();
	
	if Input.is_action_pressed("ui_up"):
		direction += Vector2(0, -1);
	if Input.is_action_pressed("ui_down"):
		direction += Vector2(0, 1);
	if Input.is_action_pressed("ui_left"):
		direction += Vector2(-1, 0);
	if Input.is_action_pressed("ui_right"):
		direction += Vector2(1, 0);
	
	return direction.rotated($Body/Physics.get_global_rotation());

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.pressed:
		$Body.attack();

func _on_Room_cleared(room):
	$Body.emit_signal("room_cleared", room);

func _on_enemy_died(enemy):
	$Body/Attributes.get_stat("CurXP").adjust_base_score(enemy.xp);

func _on_Body_stat_changed(stat, old_score, new_score):
	pass;

func is_player():
	return true;

func level_up(XPContainer):
	XPContainer.current_resource.adjust_base_score(-XPContainer.maximum_resource.get_cur_score());
	XPContainer.maximum_resource.set_base_score(XPContainer.maximum_resource.get_cur_score() * 2);