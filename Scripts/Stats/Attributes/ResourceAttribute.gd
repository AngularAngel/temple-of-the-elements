extends PrimaryAttribute

class_name ResourceAttribute

var maximum;
export(String) var _max_stat_name: String;

func _ready():
	if _max_stat_name != "":
		maximum = get_stat(_max_stat_name);

func set_cur_score(value):
	if maximum != null and value > maximum.get_cur_score():
		value = maximum.get_cur_score();
	.set_cur_score(value);

func change_base(new_base):
	if maximum != null and new_base > maximum.get_cur_score():
		new_base = maximum.get_cur_score();
	.change_base(new_base);

func replenish(increase):
	adjust_base_score(increase);