extends Node2D

# Member variables
var MeleeAttack = preload("res://Scenes/World/Entities/Attacks/MeleeAttack/MeleeAttack.tscn");
var attack_speed;
var striking_strength;

func _ready():
	var attributes = get_node("../../Attributes");
	attack_speed = attributes.get_stat("AttackSpeed");
	striking_strength = attributes.get_stat("StrikingStrength");

func attack():
	if $AttackTimer.get_time_left() > 0:
		return;
	else:
		var melee_attack = MeleeAttack.instance();
		melee_attack.set_position(Vector2(-20, -50));
		melee_attack.distance = 40;
		melee_attack.damage = 50 * striking_strength.get_cur_score();
		add_child(melee_attack);
		$AttackTimer.start(0.3 * attack_speed.get_cur_score());