extends Area2D

# Member variables
var damage;

func _on_Timer_timeout():
	queue_free();

func _on_Attack_body_entered(body):
	body.be_attacked(self);
