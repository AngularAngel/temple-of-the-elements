extends Vertex

class_name RequirementNode

export(bool) var fulfilled = false;


func fulfilled():
	return fulfilled;

func accessible():
	if edges_in.size() == 0:
		return true;
	
	for edge in edges_in:
		if edge.fulfilled(): 
			return true;
			
	return false;

func update_color():
	if fulfilled():
		if accessible():
			return $Appearance.set_texture(preload("res://Scenes/UI/Graphs/RequirementGraph/RequirementNode/RequirementNodeAcquired.png"));
		else:
			return $Appearance.set_texture(preload("res://Scenes/UI/Graphs/RequirementGraph/RequirementNode/RequirementNodeInvalid.png"));
	else:
		if accessible():
			return $Appearance.set_texture(preload("res://Scenes/UI/Graphs/RequirementGraph/RequirementNode/RequirementNodeAvailable.png"));
		else:
			return $Appearance.set_texture(preload("res://Scenes/UI/Graphs/RequirementGraph/RequirementNode/RequirementNode.png"));

func pressed():
	if fulfilled():
		fulfilled = false;
		update();
		return;
	
	if accessible():
		fulfilled = true;
		update();
		return;

func update():
	if update_color():
		for edge in edges_out:
			edge.update();

func position_inside(position):
	var transform = get_viewport().get_canvas_transform()
	return transform.xform(get_position()).distance_to(position) < 12 * transform.get_scale().x;

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed and position_inside(event.get_position()):
		pressed();
		get_tree().set_input_as_handled();