extends StatContainer

class_name ResourceContainer

signal full
signal empty

enum Frequency {NONE, TIME, ROOM_CLEARED};

var current_resource: ResourceAttribute;
var maximum_resource: Attribute;
var regen_resource: Attribute;
export(Frequency) var regen_time = Frequency.NONE;
export var set_max = true;
export var capped = true;

func _ready():
	if set_max:
		current_resource.set_base_score(maximum_resource.get_cur_score());
	if capped:
		current_resource.maximum = maximum_resource;
	
	current_resource.connect("changed", self, "_on_current_stat_changed");
	maximum_resource.connect("changed", self, "_on_maximum_stat_changed");

func _process(delta):
	if regen_time == Frequency.TIME:
		current_resource.replenish(regen_resource.get_cur_score() * delta);

func add_stat(stat):
	if stat.get_name().begins_with("Cur"):
		current_resource = stat;
	
	if stat.get_name().begins_with("Max"):
		maximum_resource = stat;
	
	if stat.get_name().ends_with("Regen"):
		regen_resource = stat;
	.add_stat(stat);

func _on_room_cleared(room):
	if regen_time == Frequency.ROOM_CLEARED:
		current_resource.replenish(regen_resource.get_cur_score());

func _on_current_stat_changed(stat, old_score, new_score):
	if new_score == maximum_resource.get_cur_score():
		emit_signal("full", self);
	if new_score <= 0:
		emit_signal("empty", self);

func _on_maximum_stat_changed(stat, old_score, new_score):
	var percent_change = new_score/old_score;
	current_resource.set_base_score(current_resource.get_cur_score() * percent_change);