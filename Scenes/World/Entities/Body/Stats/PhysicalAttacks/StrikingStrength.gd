extends PercentileAttribute

func _ready():
	add_dependency("Strength", 0.6);
	add_dependency("Dexterity", 0.2);
	add_dependency("Vitality", 0.2);
	recalculate();