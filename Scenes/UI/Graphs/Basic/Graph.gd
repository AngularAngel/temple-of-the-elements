extends Control

class_name Graph

var Edge = preload("res://Scenes/UI/Graphs/Basic/Edge/Edge.tscn");

func _ready():
	create_graph();
	
	$Vertexes.raise();

func link_vertexes(start, end, edge = Edge.instance()):
	edge.set_start(start);
	edge.set_end(end);
	$Edges.add_child(edge);

func create_graph():
	link_vertexes($Vertexes/Vertex, $Vertexes/Vertex2);
	link_vertexes($Vertexes/Vertex2, $Vertexes/Vertex3);