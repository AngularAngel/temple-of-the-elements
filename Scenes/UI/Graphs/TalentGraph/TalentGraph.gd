extends RequirementGraph

var CharacterNode = preload("res://Scenes/UI/Graphs/TalentGraph/TalentNode.tscn");
var angle = 0;
var arc = PI * 2.0 / 5.0;

func create_graph():
	pass;

func initialize_graph(stats):
	for i in range(5):
		var talent_tree = TalentTree.new();
		$TalentTrees.add_child(talent_tree);
		talent_tree.generate_talents(stats, 5);
	for tree in $TalentTrees.get_children():
		create_tree(tree.talents);
		angle += arc;
	
func create_tree(tree):
	var end_nodes = [];
	
	var i = 0;
	for layer_def in tree:
		end_nodes = create_layer(layer_def, i, end_nodes);
		i += 1;

func create_layer(layer_def, layer_num, prev_layer_ends = []):
	var cluster_ends = [];
	
	var i = 0;
	for cluster_def in layer_def:
		var cluster_angle = angle - arc / 2.0 + (0.5 + i) * arc / layer_def.size();
		
		var prev_ends = [];
		if i > 0:
			prev_ends.append(prev_layer_ends[i - 1]);
		if i < prev_layer_ends.size():
			prev_ends.append(prev_layer_ends[i]);
			
		cluster_ends.append(create_cluster(cluster_def, layer_num, cluster_angle, prev_ends));
		i += 1;
	
	return cluster_ends;

func create_cluster(cluster_def, layer, angle, roots = []):
	var entry_position = Vector2();
	entry_position.x = (45.0 + 245.0 * layer) * cos(angle);
	entry_position.y = (45.0 + 245.0 * layer) * sin(angle);
	var entry_vertex = new_vertex(cluster_def.entry_talent, entry_position);
	
	for root in roots:
		link_vertexes(root, entry_vertex);
		
	#We need to keep track of these to link them to the end talent.
	#Also, we'll use them as a count of how many talent lines we've been through.
	var line_ends = [];
	
	for talent_line in cluster_def.talent_lines:
		var last_vertex = entry_vertex;
		var line_position = Vector2(entry_position.x, entry_position.y);
		
		var line_angle = angle;
		if line_ends.size() < (cluster_def.talent_lines.size() - 1.0)/2.0:
			line_angle -= PI / 2.0;
		else:
			if line_ends.size() > (cluster_def.talent_lines.size() - 1.0)/2.0:
				line_angle += PI / 2.0;
		
		var line_displacement = 40.0 * abs(((cluster_def.talent_lines.size() - 1.0)/2.0) - line_ends.size());
		
		line_position.x += line_displacement * cos(line_angle);
		line_position.y += line_displacement * sin(line_angle);

		var line_num = 0;
		for talent_def in talent_line:
			line_position.x += 45.0 * cos(angle);
			line_position.y += 45.0 * sin(angle);
			line_num += 1;
			
			var line_vertex = new_vertex(talent_def, line_position);
			link_vertexes(last_vertex, line_vertex);
			last_vertex = line_vertex;
		
		line_ends.append(last_vertex);
	
	var end_position = Vector2(entry_position.x, entry_position.y);
	end_position.x += 45.0 * 4 * cos(angle);
	end_position.y += 45.0 * 4 * sin(angle);
	
	var end_vertex = new_vertex(cluster_def.end_talent, end_position);
	for line_end in line_ends:
		link_vertexes(line_end, end_vertex);
	
	return end_vertex;

func new_vertex(talent = null, position = Vector2(0, 0)):
	var vertex = CharacterNode.instance();
	vertex.set_position(position);
	if talent != null:
		vertex.add_child(talent);
	$Vertexes.add_child(vertex);
	return vertex;

func fork_vertex(vertex, forks = 2):
	var new_vertexes = [];
	
	for i in range(forks):
		new_vertexes.append(new_vertex());
		link_vertexes(vertex, new_vertexes[i]);
	
	return new_vertexes;

func fork_tree(depth, forks = 2):
	rand_seed(OS.get_unix_time());
	var graph = [[new_vertex()]];
	
	for i in range(depth):
		graph.append([]);
		
		for vertex in graph[i]:
			graph[i + 1] += fork_vertex(vertex, rand_range(1, forks));
	
	var max_size = graph[-1].size();
	for i in range(depth + 1):
		var j = 0.5;
		var size = graph[i].size();
		
		for vertex in graph[i]:
			vertex.set_position(Vector2(20 + i * 50, 300 - max_size * 12.5 + j * (max_size/size) * 25));
			j += 1;