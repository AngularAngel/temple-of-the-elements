extends PercentileAttribute

func _ready():
	add_dependency("Strength", 0.15);
	add_dependency("Dexterity", 0.3);
	add_dependency("Agility", 0.3);
	add_dependency("Vitality", 0.15);
	add_dependency("Spirit", 0.1);
	recalculate();