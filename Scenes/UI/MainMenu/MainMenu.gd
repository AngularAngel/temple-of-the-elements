extends Control

signal started_new_game

func _on_NewGameButton_pressed():
	emit_signal("started_new_game");


func _on_ExitButton_pressed():
	get_tree().quit();
