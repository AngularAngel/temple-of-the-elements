extends RigidBody2D

# Member variables
var max_speed;
var movement = Vector2(0, 0);
var turn = 0;

func _ready():
	max_speed = $"../Attributes".get_stat("MaxSpeed");
	
func _integrate_forces(state):
	var speed = state.get_linear_velocity();
	if speed.length() > max_speed.get_cur_score():
		speed = speed.clamped(max_speed.get_cur_score());
		state.set_linear_velocity(speed);
		
	if movement.length() > 0:	
		
		if speed.length() > 0:
			var direction_change = speed.angle_to(movement);
			
			var oldmove = movement;
			
			if (abs(direction_change) > 0):
				movement *= (1 + speed.project(movement).length() / max_speed.get_cur_score());
			else:
				movement *= (1 - speed.project(movement).length() / max_speed.get_cur_score());
		
		state.apply_central_impulse(movement);
		movement = Vector2(0, 0);

func be_attacked(attack):
	$Defense.take_damage(attack.damage);

func is_player():
	return get_parent().controller.is_player();