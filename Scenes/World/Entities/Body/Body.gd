extends Node2D

class_name Body

signal died
signal stat_changed
signal room_cleared

# Member variables
var controller;

var acceleration; # Pixels/second
var turn_speed;
var turn = 0;

func _ready():
	acceleration = $Attributes.get_stat("Acceleration");
	turn_speed = $Attributes.get_stat("TurnSpeed");
	
	var sensors = [get_node("Camera"), get_node("Ray")];
	for sensor in sensors:
		if sensor != null:
			remove_child(sensor);
			$Physics.add_child(sensor);

func _physics_process(delta):
	turn += controller.get_turn();
	
	if turn != 0:
		if turn > 1: turn = 1;
		if turn < -1: turn = -1;
		
	turn *= turn_speed.get_cur_score() * delta;
		
	$Physics.movement += controller.get_direction().normalized() * acceleration.get_cur_score() * delta;
	
	$Physics.rotate(turn);

func attack():
	$Physics/Offense.attack();

func die():
	emit_signal("died", self);

func _on_Stat_changed(stat, old_score, new_score):
	emit_signal("stat_changed", stat, old_score, new_score);