extends SecondaryAttribute

func _ready():
	add_dependency("Strength", 0.1);
	add_dependency("Agility", 0.6);
	add_dependency("Vitality", 0.2);
	add_dependency("Spirit", 0.1);
	multiplier = 5
	recalculate();