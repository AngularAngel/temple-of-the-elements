extends HBoxContainer

var resource: ResourceContainer setget set_resource;

func set_resource(value):
	resource = value;
	resource.current_resource.connect("changed", self, "_on_current_stat_changed");
	resource.maximum_resource.connect("changed", self, "_on_maximum_stat_changed");
	$ResourceBar.set_value(resource.current_resource.get_cur_score());
	$ResourceBar.set_max(resource.maximum_resource.get_cur_score());
	update_score();

func update_score():
	$ResourceScore.set_text(resource.current_resource.get_display_score(3) + "/" + resource.maximum_resource.get_display_score(3));

func _on_current_stat_changed(stat, old_score, new_score):
	$ResourceBar.set_value(resource.current_resource.get_cur_score());
	update_score();

func _on_maximum_stat_changed(stat, old_score, new_score):
	$ResourceBar.set_max(resource.maximum_resource.get_cur_score());
	update_score();