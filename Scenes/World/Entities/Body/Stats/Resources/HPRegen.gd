extends SecondaryAttribute

func _ready():
	add_dependency("Vitality", 0.5);
	add_dependency("Spirit", 0.25);
	add_dependency("MaxHP", 0.25);
	multiplier = 0.2
	recalculate();