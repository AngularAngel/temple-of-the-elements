extends Attribute

class_name PotentialAttribute

var parent;

func _ready():
	parent = get_parent();
	parent.connect("changed", self, "_on_Dependency_changed");

func display_score(score, max_length = 10, min_length = 0):
	return parent.display_score(score, max_length, min_length);

func stat_sum():
	var sum = 0.0;
	for dependency in parent.dependencies.keys():
		sum += dependency.potential.get_cur_score() * parent.dependencies[dependency];
	return sum;

func recalculate():
	var temp_score = calc_cur_score(parent.get_base_potential());
	
	var cur_score = get_cur_score();
	if cur_score != temp_score:
		var old_score = cur_score;
		set_cur_score(parent.get_base_potential());
		emit_signal("changed", self, old_score, get_cur_score());

func _on_Dependency_changed(stat, old_score, new_score):
	recalculate();