extends SecondaryAttribute

func _ready():
	add_dependency("Spirit", 0.6);
	add_dependency("Focus", 0.25);
	add_dependency("Vitality", 0.15);
	recalculate();