extends PanelContainer

class_name CharacterPanel

export var action: String;
var _action_processing = false;
var hud;

func _on_CloseButton_pressed():
	close();

func _process(delta):
	if Input.is_action_pressed("ui_cancel"):
		close();
	if Input.is_action_pressed(action):
		if (!_action_processing):
			switch_display();
			_action_processing = true;
	else:
		_action_processing = false;

func close():
	hide();
	get_parent().close_panel(self);

func open():
	show();
	get_parent().open_panel(self);

func make_right():
	$ScreenLayout/TitleBar.move_child($ScreenLayout/TitleBar/CloseButton, 0);
	hide();
	show();

func make_left():
	$ScreenLayout/TitleBar.move_child($ScreenLayout/TitleBar/CloseButton, 1);

func switch_display():
	if is_visible():
		close();
	else:
		open();